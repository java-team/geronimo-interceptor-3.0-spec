geronimo-interceptor-3.0-spec (1.0.1-5) unstable; urgency=medium

  * Team upload.
  * Drop README.Debian since the information is at best relevant for
    package developers but does not have any user related information
  * Build-Depends: s/default-jdk/default-jdk-headless/
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Trim trailing whitespace.
  * Update watch file format version to 4.
  * Use secure URI in Homepage field.
  * Install NOTICE.txt file
  * d/copyright: DEP5
  * Do not mention license in long description
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Wed, 12 Feb 2025 20:25:30 +0100

geronimo-interceptor-3.0-spec (1.0.1-4) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with Java 9 (Closes: #893153)
  * Build with Maven instead of Ant
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 4.1.3
  * Switch to debhelper level 11
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 23 Mar 2018 00:43:22 +0100

geronimo-interceptor-3.0-spec (1.0.1-3) unstable; urgency=medium

  * Team upload.
  * Added the missing javax.interceptor.Interceptor and InterceptorBinding
    annotations.

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 21 Oct 2015 21:52:03 +0200

geronimo-interceptor-3.0-spec (1.0.1-2) unstable; urgency=medium

  * Team upload.
  * Moved the package to Git (Closes: #669298, #669418)
  * Install the Maven artifacts
  * debian/control:
    - Removed the runtime dependency on the JRE
    - Removed the XSBC-Original-Maintainer field (Closes: #573506)
    - Removed the deprecated DM-Upload-Allowed field
    - The package is now maintained by the Debian Java Maintainers
    - Standards-Version updated to 3.9.6 (no changes)
  * Switch to debhelper level 9
  * Switch to source format 3.0 (quilt)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 02 Jul 2015 22:58:36 +0200

geronimo-interceptor-3.0-spec (1.0.1-1) unstable; urgency=low

  * Port package to pkg-java based largely on existing Ubuntu package

 -- Chris Grzegorczyk <grze@eucalyptus.com>  Wed, 16 Dec 2009 21:47:25 -0800

geronimo-interceptor-3.0-spec (1.0.1-0ubuntu1) karmic; urgency=low

  * Initial release. New Eucalyptus dependency.

 -- Thierry Carrez <thierry.carrez@ubuntu.com>  Mon, 27 Jul 2009 11:15:06 +0200
